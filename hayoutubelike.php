<?php
	/*
	 * Plugin Name: HA Youtube-like Playlist
	 * Version: 0.1
	 * Plugin URI: http://hemantarora.com/
	 * Description: The first YouTube-like playlist plugin for wordpress.
	 * Author: Hemant Arora
	 * Author URI: http://hemantarora.com/
	 */
	define('HAYTLP_PREFIX', 'haytlp_');
	
	add_action('admin_menu', 'ha_youtubelike_menu');
	function ha_youtubelike_menu() {
		add_menu_page(__('HA Youtube-like Playlist', 'hayoutubelike'), __('Youtube Playlist', 'hayoutubelike'),
			10, 'hayoutubelike', 'hayoutubelike_admin_page', plugins_url('hayoutubelike/images/hayoutubelike_16.png'));
	}
	function hayoutubelike_admin_page() {
		if($_REQUEST['act'] == 'do_save_youtube_ids') {
			$video_ids = array();
			foreach($_REQUEST['haylp_video_id'] as $haylp_video_id)
				if(trim($haylp_video_id) != '')
					array_push($video_ids, trim($haylp_video_id));
			update_option(HAYTLP_PREFIX.'video_ids', $video_ids);
			update_option(HAYTLP_PREFIX.'playlist_title', $_REQUEST['haylp_playlist_title']);
			update_option(HAYTLP_PREFIX.'google_api_key', $_REQUEST['haylp_google_api_key']);
			update_option(HAYTLP_PREFIX.'playlist_autoplay', $_REQUEST['haylp_playlist_autoplay']);
		}
		$video_ids = get_option(HAYTLP_PREFIX.'video_ids');
		$playlist_title = get_option(HAYTLP_PREFIX.'playlist_title');
		$google_api_key = get_option(HAYTLP_PREFIX.'google_api_key');
		$playlist_autoplay = get_option(HAYTLP_PREFIX.'playlist_autoplay');
		?><div class="wrap">
			<div class="icon32" id="icon-hayoutubelike"><br /></div>
			<h2>HA Youtube-like Playlist - Settings</h2>
			<p></p>
			<?php if(count($video_ids) > 0 && is_array($video_ids)) { ?>
			<p>Use the following shortcode in your posts or pages where you want the Youtube-like Playlist to appear:<br />
				<textarea style="background-color: #EEE; border: 0 none; font-family: Consolas,Monaco,monospace; font-size: 11px; width: 100%;" readonly="readonly" onclick="jQuery(this).select();">[ha_youtubelike_playlist<?php echo $playlist_autoplay == '1' ? ' autoplay="1"' : ''; echo $playlist_title ? ' playlist_title="'.$playlist_title.'"' : ''; ?> youtube_video_ids="<?php echo implode('|', $video_ids); ?>"]</textarea>
			</p>
			<p>OR use the PHP function <code>the_ha_youtubelike_playlist()</code> to display the playlist in your theme with the settings saved below.</p>
			<?php } ?>
			<form name="haylp_frm_youtube_ids" id="haylp_frm_youtube_ids" method="post" action="">
				<input type="hidden" name="act" value="do_save_youtube_ids" />
				<h4>Playlist Title:</h4>
				<p>
					<input type="text" name="haylp_playlist_title" class="haylp_playlist_title" value="<?php echo $playlist_title; ?>" />
					<span class="clear"></span>
				</p>
				<h4>Google API Key <a href="https://console.cloud.google.com/apis/" target="_blank" style="font-weight: normal">(Get it here)</a>:</h4>
				<p>
					<input type="text" name="haylp_google_api_key" class="haylp_google_api_key" value="<?php echo $google_api_key; ?>" />
					<span class="clear"></span>
				</p>
				<h4>Playback Options:</h4>
				<p>
					<input type="checkbox" name="haylp_playlist_autoplay" class="haylp_playlist_autoplay" value="1"<?php echo $playlist_autoplay == '1' ? ' checked="checked"' : ''; ?> />
					<span class="haylp_label">AutoPlay the videos</span>
					<span class="clear"></span>
				</p>
				<h4>Manage YouTube Video IDs:<br /><span class="description" style="font-weight: normal; color: #AAA; font-style: normal">Enter the YouTube video ids below, to add new videos to the playlist.<br /><br />For example:<br />If the YouTube video link is: <span style="border-bottom: #AAA 1px dotted;">https://www.youtube.com/watch?v=<span style="color: #333;">AbCdEfGh_Ij</span></span><br />You should add &#0147;<span style="color: #333;">AbCdEfGh_Ij</span>&#0148; in the box below.</span></h4>
				<ul id="sortable">
				<?php if(count($video_ids) > 0 && is_array($video_ids)) {
					foreach($video_ids as $video_id) { ?>
					<li>
						<p>
							<span class="haylp_label">YouTube Video ID:</span>
							<input type="text" name="haylp_video_id[]" value="<?php echo $video_id; ?>" />
							<span class="clear"></span>
						</p>
					</li>
				<?php }
				} else { ?>
					<li>
						<p>
							<span class="haylp_label">YouTube Video ID:</span>
							<input type="text" name="haylp_video_id[]" />
							<span class="clear"></span>
						</p>
					</li>
				<?php } ?>
				</ul>
				<p class="submit">
					<input type="submit" value="Save Changes" class="button-primary" />
					<input type="button" value="Add Video" class="button add" />
				</p>
			</form>
		</div><?php
	}
	
	add_action('admin_head', 'ha_youtubelike_admin_head');
	function ha_youtubelike_admin_head() {
		?><style>
		#icon-hayoutubelike.icon32 { background-image: url('<?php echo plugins_url('hayoutubelike/images/hayoutubelike_32.png'); ?>'); }
		#haylp_frm_youtube_ids .haylp_playlist_title,
		#haylp_frm_youtube_ids .haylp_google_api_key { width: 350px; }
		#haylp_frm_youtube_ids ul { padding: 0; margin: 0; width: 270px }
		#haylp_frm_youtube_ids li { padding: 6px 0; margin: 0; border: transparent 1px solid; }
		#haylp_frm_youtube_ids li p { padding: 0; margin: 0; }
		#haylp_frm_youtube_ids li p .haylp_label { display: block; float: left; width: 120px; line-height: 24px; padding-left: 10px; cursor: move; }
		#haylp_frm_youtube_ids li p input[type=text] { float: left; }
		#haylp_frm_youtube_ids li.ui-sortable-helper { border-radius: 3px; box-shadow: 0 0 3px #CCC; background-color: #FFF; }
		#haylp_frm_youtube_ids li.ui-sortable-placeholder { border: #CCC 1px dashed }
		.clear { clear: both; display: block; }
		</style>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('#haylp_frm_youtube_ids input.add').click(function() {
					jQuery(this).parent().parent().find('#sortable').append('<li><p><span class="haylp_label">YouTube Video ID:</span><input type="text" name="haylp_video_id[]" /><span class="clear"></span></p></li>');
				});
				jQuery( "#sortable" ).sortable();
				//jQuery( "#sortable" ).disableSelection();
			});
		</script><?php
	}
	
	add_action('admin_enqueue_scripts', 'ha_youtubelike_enqueue_scripts');
	function ha_youtubelike_enqueue_scripts() {
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-mouse');
		wp_enqueue_script('jquery-ui-sortable');
		wp_enqueue_script('jquery-ui-draggable');
		wp_enqueue_script("jquery-ui-droppable");
	}
	
	add_action('wp_footer', 'ha_youtubelike_head');
	function ha_youtubelike_head() {
		?><script type="text/javascript">
		var haytlp_debug = false;
		
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/player_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		
		var haytl_player, haytl_player_is_ready = false, haytl_player_autoplay;
		function onYouTubePlayerAPIReady() {
			haytl_player_is_ready = true;
			jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li:first-child a').click();
		}
		
		function haytlp_onPlayerReady() {
			jQuery('#ha_youtubelike_playlist .haytlp_playlist ol').height(jQuery('#ha_youtubelike_playlist_player').height() - 27);
			haytlp_log('player ready');
		}
		
		function haytlp_onPlayerStateChange() {
			haytlp_log('player changed');
			if(haytl_player.getPlayerState() == 1)
				haytl_player_autoplay = true;
			if(haytl_player.getPlayerState() == 0)
				haytl_player_next_video();
		}
		
		jQuery(document).ready(function() {
			
			jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li a').click(function() {
				if(typeof(jQuery(this).attr('data-youtubevideoid')) != 'undefined') {
					jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li').removeClass('playing');
					jQuery(this).parent().addClass('playing');
					jQuery('#ha_youtubelike_playlist .haytlp_player .haytlp_top_bar a.title').html(jQuery(this).find('.title').html());
					haytl_player_video(jQuery(this).attr('data-youtubevideoid'));
				}
			});
			
		});
		
		function haytl_player_video(video_id) {
			autoplay = (typeof(autoplay) == 'undefined') ? false : true;
			if(haytl_player_is_ready == true
			&& typeof(haytl_video_ids) != 'undefined'
			&& haytl_video_ids[0] != '') {
				if(typeof(haytl_player) != 'undefined')
					haytl_player.destroy();
				haytl_player = new YT.Player('ha_youtubelike_playlist_player', {
					height: '585',
					width: '960',
					videoId: video_id,
					playerVars: {
						autoplay: haytl_player_autoplay ? '1' : '0'
						/*listType:'playlist',
						list: 'PLTon7J1fqcir8-IjrrWz99RqqfDCG3W_3'*/
					},
					events: {
						'onReady': haytlp_onPlayerReady,
						'onStateChange': haytlp_onPlayerStateChange
					}
				});
			} else
				haytlp_log('HA Youtube-like Playlist Video "'+video_id+'" initialization failed.')
		}
		
		function haytl_player_next_video() {
			var next_video_li;
			if(jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li.playing').length > 0) {
				if(jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li.playing') == jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li:last-child'))
					next_video_li = jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li:first-child');
				else
					next_video_li = jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li.playing').next();
			} else
				next_video_li = jQuery('#ha_youtubelike_playlist .haytlp_playlist ol li:first-child');
			jQuery(next_video_li).find('a.list_video').click();
		}
		
		function haytlp_log(msg) {
			if(typeof(haytlp_debug) != 'undefined'
			&& haytlp_debug == true)
				console.log(msg);
		}
		</script><?php
	}
	
	add_action('wp_head', 'ha_youtubelike_wp_head');
	function ha_youtubelike_wp_head() { ?>
		<style>
			#ha_youtubelike_playlist { background-color: #1b1b1b; width: 1000px; margin: 50px auto; }
			#ha_youtubelike_playlist .haytlp_player { width: 640px; float: left; clear: none; }
			#ha_youtubelike_playlist .haytlp_playlist { width: 360px; float: left; clear: none; position: relative; }
			.clear { clear: both; }
			#ha_youtubelike_playlist #ha_youtubelike_playlist_player { margin: 0 !important; }
			#ha_youtubelike_playlist .haytlp_top_bar { color: #999999; font-size: 11px; height: 34px; line-height: 34px; padding: 0 8px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; word-wrap: normal; }
			#ha_youtubelike_playlist .haytlp_top_bar a.title { color: #FFF; font-size: 15px; padding: 0 8px; text-decoration: none; }
			#ha_youtubelike_playlist .haytlp_playlist .haytlp_top_bar { border-left: #2b2b2b 1px solid; text-align: right; }
			#ha_youtubelike_playlist .haytlp_playlist ol { background-color: #2b2b2b; border-bottom: 27px solid #1B1B1B; height: 300px; width: 100%; overflow: hidden; margin: 0; }
			#ha_youtubelike_playlist .haytlp_playlist ol:hover { overflow-y: auto; }
			#ha_youtubelike_playlist .haytlp_playlist ol li { border-bottom: 1px solid #1D1D1D; border-top: 1px solid #393939; color: #6E6E6E; font-size: 13px; margin: 0; padding-right: 13px; text-shadow: 0 1px 0 #000000; }
			#ha_youtubelike_playlist .haytlp_playlist ol li:first-child { border-top-color: transparent; }
			#ha_youtubelike_playlist .haytlp_playlist ol li:hover { background-color: #2f2f2f; }
			#ha_youtubelike_playlist .haytlp_playlist ol li.playing { background-color: #353535; border-color: #353535; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a { color: #333333; display: block; overflow: hidden; padding: 0 5px; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a.list_video { padding: 8px 0 8px 35px; position: relative; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a .count { bottom: 0; color: #666666; display: block; font-size: 11px; line-height: 1.4em; white-space: nowrap; font-weight: bold; height: 13px; left: 12px; margin: auto 0; position: absolute; top: 0; }
			#ha_youtubelike_playlist .haytlp_playlist ol li.playing a .count { border-bottom: 6px solid transparent; border-left: 7px solid #FFFFFF; border-top: 6px solid transparent; height: 0; text-indent: 9999px; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a .thumb_wrap { box-shadow: 0 2px 4px rgba(0, 0, 0, 0.5); float: left; margin: 4px 8px 4px 0; display: inline-block; overflow: hidden; position: relative; width: 64px; height: 36px; overflow: hidden; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a .thumb_wrap .video_thumb { background-color: transparent; position: relative; height: 36px; width: 64px; display: inline-block; overflow: hidden; vertical-align: bottom; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a .thumb_wrap .video_thumb .thumb_clip { bottom: -100px; left: -100px; position: absolute; right: -100px; text-align: center; top: -100px; white-space: nowrap; word-break: normal; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a .thumb_wrap .video_thumb .thumb_clip .clip_inner { height: 100%; left: 0; position: absolute; top: 0; width: 100% }
			#ha_youtubelike_playlist .haytlp_playlist ol li a .thumb_wrap .video_thumb .thumb_clip .clip_inner img { width: 75px; display: inline-block; vertical-align: middle; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a .thumb_wrap .video_thumb .vertical_align { display: inline-block; height: 100%; vertical-align: middle; }
			#ha_youtubelike_playlist .haytlp_playlist ol li a .title { color: #AAA; font-family: sans-serif; font-size: 13px; font-weight: bold; cursor: pointer; display: inline-block; line-height: 20px; margin-bottom: 2px; max-height: 3em; overflow: hidden; white-space: pre-line; /*nowrap*/ text-overflow: ellipsis; width: 220px; }
			#ha_youtubelike_playlist .haytlp_playlist ol li.playing a .title { color: #FFF; }
			
			#ha_youtubelike_playlist.altlayout { width: 960px; background-color: transparent; margin: 20px auto; }
			#ha_youtubelike_playlist.altlayout .haytlp_player { width: 100%; }
			#ha_youtubelike_playlist.altlayout .haytlp_top_bar { padding: 0; }
			#ha_youtubelike_playlist.altlayout .haytlp_top_bar a.title { color: #333; padding: 0; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist { width: 100%; margin-top: 20px; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist .haytlp_top_bar { border: 0 none; text-align: left; padding: 0; height: 40px; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist .haytlp_top_bar a.title { font-size: 20px; line-height: 40px; padding: 0; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol { background-color: transparent; height: auto !important; overflow: visible; border: 0 none; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li { width: 25%; float: left; list-style: outside none; border: 0 none; padding: 0; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li:hover { background-color: transparent; box-shadow: 0 0 10px rgba(0,0,0,0.2) inset }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li.playing { background-color: transparent; box-shadow: 0 0 10px rgba(0,0,0,0.5) inset }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li a { padding: 10px 0; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li a .count { display: none; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li a .title { width: 192px; display: block; margin: 0 auto; overflow: hidden; text-overflow: ellipsis; text-shadow: none; white-space: nowrap; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li:hover a .title { color: #666 }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li.playing a .title { color: #333 }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li a .thumb_wrap { display: block; float: none; margin: 4px auto; }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li a .thumb_wrap,
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li a .thumb_wrap .video_thumb { width: 192px; height: 108px }
			#ha_youtubelike_playlist.altlayout .haytlp_playlist ol li a .thumb_wrap .video_thumb .thumb_clip .clip_inner img { width: 198px; opacity: 1 !important; }
		</style>
	<?php }
	
	function ha_youtubelike_get_video_data($video_id) {
		if(trim($video_id) == '') return false;
		$saved_video_data = get_option(HAYTLP_PREFIX.'data_'.$video_id);
		if($saved_video_data)
			return $saved_video_data;
		else {
			/* OLD GOOGLE API METHOD - SHUT DOWN BY GOOGLE
			$request = 'http://gdata.youtube.com/feeds/api/videos/'.$video_id;
			$response = file_get_contents($request);
			$doc = new DOMDocument();
			if(@$doc->loadXML($response)) {
				$entry = $doc->getElementsByTagName('entry');
				$entry = $entry->item(0);
				if($entry) {
					$data = array();
					$data['title'] = $entry->getElementsByTagName('title');
					$data['title'] = $data['title']->item(0);
					$data['title'] = $data['title']->nodeValue;
					$data['author'] = $entry->getElementsByTagName('author');
					$data['author'] = $data['author']->item(0);
					$data['author'] = $data['author']->getElementsByTagName('name');
					$data['author'] = $data['author']->item(0);
					$data['author'] = $data['author']->nodeValue;
					update_option(HAYTLP_PREFIX.'data_'.$video_id, $data);
					return $data;
				} else
					return false;
			} else
				return false;*/
			$google_api_key = get_option(HAYTLP_PREFIX.'google_api_key', '');
			$request = 'https://www.googleapis.com/youtube/v3/videos?id='.$video_id.'&key='.$google_api_key.'&part=snippet';
			$response = file_get_contents($request);
			try {
				$json_response = json_decode($response);
				$video_data = $json_response->items[0];
				if(empty($video_data)) throw new Exception('Failed to retrieve video information.');
				$data = array(
					'title' => $video_data->snippet->title,
					'author' => $video_data->snippet->channelTitle
				);
				update_option(HAYTLP_PREFIX.'data_'.$video_id, $data);
				return $data;
			} catch(Exception $e) {
				return false;
			}
		}
	}
	
	add_shortcode('ha_youtubelike_playlist', 'ha_youtubelike_playlist');
	function ha_youtubelike_playlist( $atts ) {
		extract( shortcode_atts( array(
			'id' => '0',
			'playlist_title' => '',
			'youtube_video_ids' => '',
			'autoplay' => '0',
			'altlayout' => 'false'
		), $atts ) );
		
		$youtube_video_ids = explode('|', $youtube_video_ids);
		
		ob_start(); ?>
		<script type="text/javascript">
			var haytl_video_ids = new Array('<?php echo implode("', '", $youtube_video_ids); ?>');
			var haytl_player_autoplay = <?php echo $autoplay == '1' ? 'true' : 'false'; ?>;
		</script>
		<div id="ha_youtubelike_playlist"<?php echo $altlayout == 'true' ? ' class="altlayout"' : ''; ?>>
			<div class="haytlp_player">
				<div class="haytlp_top_bar"><a class="title" href="javascript: void(0)">Youtube playlist is loading...</a></div>
				<div id="ha_youtubelike_playlist_player"></div>
			</div>
			<div class="haytlp_playlist">
				<div class="haytlp_top_bar"><a class="title" href="javascript: void(0)"><?php echo $playlist_title; ?></a></div>
				<ol>
				<?php $video_index = 0;
				foreach($youtube_video_ids as $youtube_video_id) {
					$video_data = ha_youtubelike_get_video_data($youtube_video_id);
					if($video_data) { ?>
					<li>
						<a class="list_video" href="javascript: void(0)" data-youtubevideoid="<?php echo $youtube_video_id; ?>" title="<?php echo $video_data['title']; ?>">
							<span class="count"><?php echo ++$video_index; ?></span>
							<span class="thumb_wrap">
								<span class="video_thumb">
									<span class="thumb_clip">
										<span class="clip_inner">
											<img src="http://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/<?php echo $altlayout == 'true' ? '0' : '1'; ?>.jpg" />
											<span class="vertical_align"></span>
										</span>
									</span>
								</span>
							</span>
							<span class="title"><?php echo $video_data['title']; ?></span>
						</a>
					</li>
				<?php } else { ?>
					<!-- Failed to load meta data for YouTube Video ID: <?php echo $youtube_video_id; ?> --><?php }
				} ?>
				</ol>
			</div>
			<div class="clear"></div>
		</div>
		<?php return ob_get_clean();
	}
	
	function the_ha_youtubelike_playlist() {
		$video_ids = get_option(HAYTLP_PREFIX.'video_ids');
		$playlist_title = get_option(HAYTLP_PREFIX.'playlist_title');
		$playlist_autoplay = get_option(HAYTLP_PREFIX.'playlist_autoplay');
		
		echo ha_youtubelike_playlist( array(
			'playlist_title' => $playlist_title,
			'youtube_video_ids' => implode('|', $video_ids),
			'autoplay' => $playlist_autoplay
		) );
	}
?>